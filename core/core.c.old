#include "def.h"
#include "queue.h"
#include <stdio.h>
#include <stddef.h>
#include <time.h>
#include <fcntl.h>
#include <assert.h>

#define FM_LOAD   0
#define FM_UNLOAD 1
#define FM_OPENED 2
#define FM_CLOSED 3
#define FM_ERROR  4
#define FM_URG    5	//Urgent data
#define FM_SNDRDY 6 	//Send ready
#define FM_TIMER  7 	//Timer
#define FM_INPUT  8
#define FM_OUTPUT 9
#define FM_USER   16

#define dword_t unsigned int

#pragma pack(push,1)
typedef struct{
	dword_t mid; 
	dword_t id;
	dword_t len;
	unsigned char buf[0x1000];
} iobuf_t;
#pragma pack(pop)

typedef struct {
	int 		id;
	void** 		mem;
	unsigned long arg1;
	unsigned long arg2;//void*/int
} flea_entry_param_t;

typedef int(*flea_entry_proc)(flea_entry_param_t*);

typedef struct {
	int 		mid;
	flea_entry_proc proc;
	void*		mem;
	int			timeout;
	int			timepas;
} flea_module_t;

typedef struct { //回复类型的事件
	int mid;
	int id;
} reply_t;

QUEUE_DECLARE(replyque_t, reply_t, 0x100);

typedef struct {
	flea_module_t 	modules[0x100];
	flea_entry_param_t param;
	replyque_t		replyque;
	iobuf_t			inbuf;
	iobuf_t			outbuf;
	int				curmdlidx;
	int				module;
	int				sock;
	int				recvlen;
	int				sendlen;
} flea_context_t;

flea_context_t __context_block;

#define ENTRY_PARAM(_id, _mem, _arg1, _arg2) {\
	flea_entry_param_t *__param = &(__context()->param);\
	__param->id = (_id);\
	__param->mem = (_mem);\
	__param->arg1 = (_arg1);\
	__param->arg2 = (_arg2);\
}


int flea_init(int sock);
int flea_destroy();
int flea_open();
int flea_close();
int flea_load_module(int mid, flea_entry_proc proc);
int flea_unload_module(int mid);
void flea_timer_start(int msec);
void flea_timer_stop();
int flea_run();

static void 		__hexprint(unsigned char *p, int size);
static int 			__module_idx(int mid);
static unsigned long __tickcount();
static void 		__do_proc(flea_entry_proc, int);
static flea_context_t *__context();


/*		--------
 */
static void __hexprint(unsigned char *p, int size) {
	int i;
	printf("%%(%d) ", size);
	for(i=0;i<size;i++) {
		printf("%02hhX ", p[i]);
	}   
	putchar('\n');
}

static int __module_idx(int mid) {
	int i,j;
	flea_context_t *context;
	
	context = __context();
	for(i=0,j=0; j<context->module; i++) {
		if(context->modules[i].mid != 0) {
			if(context->modules[i].mid == mid)
				return i;
			j++;
		}
	}
	return -1;
}

static unsigned long __tickcount() {
	struct timespec ts;  
	clock_gettime(CLOCK_MONOTONIC, &ts);  
	printf("clock_gettime() ret %ld\n", (ts.tv_sec * 1000L + ts.tv_nsec / 1000000));
	return (ts.tv_sec * 1000L + ts.tv_nsec / 1000000);  
}

static int __checktimer(int pass) {
	int i,j;
	int minleft = -1;
	flea_context_t *context;
	
	context = __context();
	printf("pass = %d\n", pass);
	//if(__curmdlidx == 0)
	//	return -2;
	for(i=0; j<context->module; i++) { //遍历模块
		if(context->modules[i].mid != 0) {
			if(context->modules[i].timeout != -1) { //如果当前模块启用了timer
				context->modules[i].timepas += pass; //经过时间累加
				if(context->modules[i].timepas >= context->modules[i].timeout) {
					ENTRY_PARAM(FM_TIMER, context->modules[i].mem, 0, 0);
					__do_proc(context->modules[i].proc, i);
					context->modules[i].timepas = 0;
					if(minleft == -1 || minleft > context->modules[i].timeout) {
						minleft = context->modules[i].timeout;
					}
				} else {
					int left = context->modules[i].timeout - context->modules[i].timepas;
					if(minleft == -1 || minleft > left)
						minleft = left;
				}
			}
			j++;
		}
	}
	return minleft;
}

static void __do_proc(flea_entry_proc proc, int mdlidx) {
	reply_t rep;
	flea_context_t *context;
	int ret,msgid;
	
	context = __context();

	msgid = context->param.id;
	context->curmdlidx = mdlidx;
	ret = proc(&context->param);
	context->curmdlidx = 0;//Handler执行完毕后，将当前模块ID置0,避免在Handler外部调用Timer相关函数

	switch(msgid) {
		case FM_UNLOAD:
			break;
		case FM_SNDRDY:
			switch(ret) {
				case -1:
					return;
				default:
					context->outbuf.len = context->param.arg1;
					context->outbuf.id = context->param.id;
					//__outbuf.mid = mid;
					context->sendlen = 0;
			}
		case FM_LOAD:
		default:
			switch(ret) {
				case 0: 
					break;
				case 1:
					rep.id = context->param.id;
					rep.mid = context->modules[mdlidx].mid;
					printf("mdlidx = %d\n", mdlidx);
					printf("QUEUE_LEN = %d\n", QUEUE_LEN(context->replyque));
					printf("context->do_proc->rep %d %d\n", rep.mid, rep.id);
					QUEUE_EN(context->replyque, rep);// Must available
					break;
			}
	}
}

static flea_context_t *__context() {
	return &__context_block;
}

int flea_init(int sock) {
	int i;
	flea_context_t *context;

	context = __context();

	context->sock 		= sock;
	context->recvlen 	= -1;
	context->sendlen 	= -1;
	context->module 	= 0;
	QUEUE_CLR(context->replyque);
}


int flea_open() {
    struct sockaddr_in addr;
	int flags;
	flea_context_t *context;

	context = __context();

	bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(6666);
    inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);

	if (connect(context->sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        return -1;
    }
	flags = fcntl(context->sock, F_GETFL, 0); 
	fcntl(context->sock, F_SETFL, flags | O_NONBLOCK);
	return 0;
}

int flea_close() {
	
}

int flea_load_module(int mid, flea_entry_proc proc) {
	int i,j,k = -1;
	flea_context_t *context;
	
	context = __context();
	printf("fela_load_module()\n");

	if(mid <= 0) return -1;
	if(context->module >= sizeof(context->modules)/sizeof(context->modules[0])) return -1;

	for(i=0,j=0; j<context->module; i++) {
		if(context->modules[i].mid == 0)
			k = i;
		else {
			if(context->modules[i].mid == mid)
				return -1;//Module already exists
			j++;
		}
	}
	puts("m4");
	if(k == -1) k = i;

	context->modules[k].mid = mid;
	context->modules[k].proc = proc;
	context->modules[k].timeout = -1;
	context->modules[k].timepas = 0;
	context->module ++;
	//__curmdlidx = k;
	ENTRY_PARAM(FM_LOAD, &context->modules[k].mem, 0L, 0L);
	__do_proc(proc, k);
	printf("load_module count = %d\n", context->module);
	return 0;
}


int flea_unload_module(int mid) {
	int idx;
	flea_context_t *context;
	
	context = __context();
	idx = __module_idx(mid);
	if(idx != -1) {
		//__curmdlidx = idx;
		ENTRY_PARAM(FM_UNLOAD, &context->modules[idx].mem, 0L, 0L);
		context->modules[idx].proc(&context->param);
		// call "handlefinish"
		context->modules[idx].mid = 0;
		__do_proc(context->modules[idx].proc, idx);
		return 0;
	}
	return -1;//Module 'mid' not found

}

void flea_timer_start(int msec) {
	flea_context_t *context;
	assert(context->curmdlidx != 0);
	context = __context();
	context->modules[context->curmdlidx].timeout = msec;
	
}

void flea_timer_stop() {
	//assert(__curmdlidx != -1);
	flea_context_t *context;
	
	context = __context();
	context->modules[context->curmdlidx].timeout = -1;
}

int flea_run() {
	struct timeval* ptimeout, timeout={0,0};
	flea_context_t *context;
	fd_set infds, outfds, *pinfds, *poutfds;

	context = __context();
	ptimeout = &timeout;
	for(;;) {
		int maxfd,ret;
		unsigned long tick;
		printf("[*] LOOP\n");

		tick = __tickcount();
		FD_ZERO(&infds); 
		FD_SET(context->sock, &infds);

		FD_ZERO(&outfds); 
		FD_SET(context->sock, &outfds);

		maxfd = context->sock+1;

		pinfds = (QUEUE_LEN(context->replyque) == QUEUE_SIZE(context->replyque)) ? NULL : &infds;
		poutfds = QUEUE_ISEMPTY(context->replyque) ? NULL : &outfds;

		if(ptimeout) {
			printf("ptimeout->(sec %lu usec %lu)\n", (unsigned long)ptimeout->tv_sec, (unsigned long)ptimeout->tv_usec);
		}
		printf("select %p %p %p\n", pinfds, poutfds, ptimeout);
		ret = select(maxfd, pinfds, poutfds, NULL, ptimeout);
		printf("select tick: %ld\n", __tickcount() - tick);
		switch(ret){
			case -1: //select 错误
				perror("select");
				exit(-1);
				break;
			case 0: //超时
				printf("select ret 0, timeout\n");
				break;
			default:  //可读、可写、错误
				printf("select ret %d\n", ret);
				if(FD_ISSET(context->sock,&infds)) { //处理可读
#define HEADER_SIZE offsetof(iobuf_t,buf)
					int len;
					if(context->recvlen == -1) {
						len = recv(context->sock, (void*)&context->inbuf, HEADER_SIZE, MSG_PEEK); //读取头部
						if(len >= HEADER_SIZE) {
							puts("m11");
							__hexprint((void*)&context->inbuf,len);
							len = recv(context->sock, &context->inbuf, HEADER_SIZE + context->inbuf.len, 0);
							__hexprint((void*)&context->inbuf,len);
							if(len > 0) {
								context->recvlen = len;
								printf("pkg len %d %ld\n",context->recvlen, context->inbuf.len + HEADER_SIZE);
							}
						}
					} else {
						len = recv(context->sock, 
								context->inbuf.buf + (context->recvlen - HEADER_SIZE), 
								context->inbuf.len - (context->recvlen - HEADER_SIZE), 
								0);
						if(len > 0) {
							context->recvlen += len;
						}
					}

					if(len < 0) { //任意一个recv出错
							printf("len = %d\n", len);
							perror("recv");
							goto endloop;
					} else if(len == 0) {
						int i,j;
						for(i=0,j=0; j<context->module; i++) {
							if(context->modules[i].mid != 0) {
								//__curmdlidx = i;
								ENTRY_PARAM(FM_UNLOAD, context->modules[i].mem, 0L, 0L);
								__do_proc(context->modules[i].proc, i);
								j++;
							}
						}

						printf("closed\n");

						goto endloop;
					}

					if(context->recvlen - HEADER_SIZE == context->inbuf.len) {
						puts("m3");
						int idx = __module_idx(context->inbuf.mid);
						if(idx == -1) {
							printf("Unknow module Id: %u, package thrown away.", context->inbuf.mid);
							continue;
						} else {
							//__curmdlidx = idx;
							ENTRY_PARAM((int)context->inbuf.id, &context->modules[idx].mem, (unsigned long)context->inbuf.buf, (unsigned long)context->inbuf.len);
							__do_proc(context->modules[idx].proc, idx);
							context->recvlen = -1;
						}
					} else {
						// Send not finished
					}
				} // end if
				if(poutfds && FD_ISSET(context->sock, &outfds)) { //处理可写
					if(context->sendlen == -1) {
						reply_t repl;
						int idx;
						QUEUE_DE(context->replyque, repl);
						printf("repl %d %d\n", repl.mid, repl.id);
						idx = __module_idx(repl.mid); //idx 一定存在
						printf("__module = %d, idx = %d\n", context->module, idx);
						if(idx == -1) puts("[!]AF idx != -1"), *(int*)NULL = 0;
						//__curmdlidx = idx;
						ENTRY_PARAM(FM_SNDRDY, &context->modules[idx].mem, (unsigned long)repl.id, (unsigned long)context->outbuf.buf);
						__do_proc(context->modules[idx].proc, idx);
						
					} 
					printf("__sendlen %d\n", context->sendlen);
					if(context->sendlen != -1) {
						context->sendlen += send(context->sock,
								(void*)&context->outbuf + context->sendlen,
								(context->outbuf.len + HEADER_SIZE) - context->sendlen,
								0);
						printf("__sendlen2 %d\n", context->sendlen);
						if(context->sendlen ==  HEADER_SIZE + context->outbuf.len) {
							//over
							context->sendlen = -1;
						}
					}
					
				} // end if
		}// end switch

		//timer
		int timeout_ms = __checktimer(__tickcount() - tick);
		//printf("pass = %d\n", pass);
		//exit(-1);
		printf("__checktimer ret %d\n", timeout_ms);
		if(timeout_ms == -1)
			ptimeout = NULL;
		else {
			timeout.tv_sec = timeout_ms/ 1000;
			timeout.tv_usec = timeout_ms % 1000 * 1000;
			ptimeout = &timeout;
		}
	}// end while
endloop:;
}

int test_proc(flea_entry_param_t *p) {
	switch(p->id) {
		case FM_SNDRDY:
			p->id = /*p->arg1*/8;
			p->arg1 = strlen("hello");
			strcpy((void*)p->arg2,"hello");
			printf("** SNDRDY\n");
			return 0;
		case FM_LOAD:
			printf("** LOAD\n");
			flea_timer_start(4000);
			return 1;
		case FM_UNLOAD:
			printf("** UNLOAD\n");
			break;
		case FM_TIMER:
			printf("** TIMER\n");
			break;
		default:
			printf("** test_proc(id=%d, mem=%p)\n", p->id, p->mem);
			__hexprint((void*)p->arg1, (int)p->arg2);
			p->id = 5;
			return 1;
	}
	return 0;
}

void main() {
	int sock;

	sock = socket(AF_INET, SOCK_STREAM, 0);

	flea_init(sock);
	if(flea_open() < 0) {
		perror("flea_open");
		return;
	}
	flea_load_module(1,test_proc);
	flea_run();
}





