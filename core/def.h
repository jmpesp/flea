#include <unistd.h>
#include <sys/types.h>       /* basic system data types */
#include <sys/socket.h>      /* basic socket definitions */
#include <sys/time.h>
#include <netinet/in.h>      /* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>       /* inet(3) functions */
#include <fcntl.h>

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#define NETREVERSE32(n) (\
		__BYTE_ORDER__ ==__ORDER_BIG_ENDIAN__ ?\
		(n) :\
		((n)>>24 | ((n)>>8&0xFF00) | ((n)<<8&0xFF0000) | (n)<<24))

