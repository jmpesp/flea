#include "core.h"
#include <string.h>
#include <stdio.h>
int test_proc(flea_entry_param_t *p) {
	switch(p->msgid) {
		case FM_SNDRDY:
			p->msgid = /*p->arg1*/8;
			p->arg1 = strlen("hello");
			strcpy((void*)p->arg2,"hello");
			printf("** SNDRDY\n");
			return 0;
		case FM_LOAD:
			printf("** LOAD\n");
			flea_timer_start(2000);
			return 1;
		case FM_UNLOAD:
			printf("** UNLOAD\n");
			break;
		case FM_TIMER:
			printf("** TIMER\n");
			break;
		default:
			printf("** test_proc(id=%d, mem=%p)\n", p->msgid, p->mem);
			__hexprint((void*)p->arg1, (int)p->arg2);
			p->msgid = 5;
			return 1;
	}
	return 0;
}

void main() {
	int sock;


	if(flea_init() == -1)
		return;
	if(flea_open() < 0) {
		perror("flea_open");
		return;
	}
	flea_service_load(3,test_proc);
	flea_run();
}
