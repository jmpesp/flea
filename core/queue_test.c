#include <stdio.h>
#include "queue.h"
/*
#define QUEUE_DECLARE(_type, _nodetype, _size) \
	typedef struct { \
		int rear; \
		int front; \
		_nodetype vect[(_size) + 1]; \
	} _type; 
#define QUEUE_CLR(_q) 		(void)((_q).front = 0, (_q).rear = 0);
#define QUEUE_ISEMPTY(_q)   ((_q).front == (_q).rear)
#define QUEUE_SIZE(_q)      (sizeof((_q).vect)/sizeof((_q).vect[0]) - 1)
#define QUEUE_EN(_q, _v) \
	((((_q).front + 1) % (QUEUE_SIZE(_q)+1) == (_q).rear)) ? -1 \
	: (((_q).vect[(_q).front] = (_v)), (_q).front = ((_q).front + 1) % (QUEUE_SIZE(_q)+1), 0)
#define QUEUE_DE(_q, _v) \
	((_q).front == (_q).rear) ? -1 \
	: (_v = (_q).vect[(_q).rear], ((_q).rear = ((_q).rear + 1) % (QUEUE_SIZE(_q)+1)), 0)
#define QUEUE_LEN(_q) \
	((_q).front < (_q).rear) \
	? ((QUEUE_SIZE(_q)+1) - ((_q).rear - (_q).front)) \
	: (_q).front - (_q).rear
*/
QUEUE_DECLARE(queue_int, int, 5);

int main() {
	queue_int q; int a;
	QUEUE_CLR(q);
	printf("size %ld\n",QUEUE_SIZE(q));
	printf("en 1 ret %d\n", QUEUE_EN(q,1));
	printf("en 2 ret %d\n", QUEUE_EN(q,2));
	printf("en 3 ret %d\n", QUEUE_EN(q,3));
	printf("en 4 ret %d\n", QUEUE_EN(q,4));
	printf("en 5 ret %d\n", QUEUE_EN(q,5));
	printf("en 6 ret %d\n", QUEUE_EN(q,6));
	printf("%d %d\n", q.front, q.rear);
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("%d %d\n", q.front, q.rear);
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("%d %d\n", q.front, q.rear);
	printf("len %ld\n",QUEUE_LEN(q));
	printf("en 6 ret %d\n", QUEUE_EN(q,6));
	printf("%d %d\n", q.front, q.rear);
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("%d %d\n", q.front, q.rear);
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("%d %d\n", q.front, q.rear);
	printf("de %d ret %d\n",a,QUEUE_DE(q,a));
	printf("len %ld\n",QUEUE_LEN(q));
	return 0;
}
