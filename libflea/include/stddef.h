#ifndef _STDDEF_H
#define _STDDEF_H

#if defined(__x86_64__)
#	ifndef __SIZE_TYPE__
#		define __SIZE_TYPE__ long unsigned int
#	endif
#	ifndef __WCHAR_TYPE__
#		define __WCHAR_TYPE__ int
#	endif
#	ifndef __PTRDIFF_TYPE__
#		define __PTRDIFF_TYPE__ long int
#	endif
#elif defined(__i386__) /* i386 */
#	ifndef __SIZE_TYPE__
#		define __SIZE_TYPE__ unsigned int
#	endif
#	ifndef __WCHAR_TYPE__
#		define __WCHAR_TYPE__ short int
#	endif
#	ifndef __PTRDIFF_TYPE__
#		define __PTRDIFF_TYPE__ int
#	endif
#else
#	error "Do not support arch."
#endif /* defined(x86_64__) */

typedef __SIZE_TYPE__ size_t;
typedef __PTRDIFF_TYPE__ ssize_t;
typedef __WCHAR_TYPE__ wchar_t;
typedef __PTRDIFF_TYPE__ ptrdiff_t;
typedef __PTRDIFF_TYPE__ intptr_t;
typedef __SIZE_TYPE__ uintptr_t;

#ifndef __int8_t_defined
#define __int8_t_defined
typedef signed char int8_t;
typedef signed short int int16_t;
typedef signed int int32_t;
typedef signed long long int int64_t;
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long int uint64_t;
#endif

#define NULL ((void *)0)
#define offsetof(type, field) ((size_t)&((type *)0)->field)

#endif
