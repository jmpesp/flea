/*
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 1990, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is refactored by Atanas Filipov - it.feel.filipov@gmail.com
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


char * index(const char * s, int c)
{assert(s);

    return strchr(s, c);
}

char * rindex(const char * s, int c)
{assert(s);

    return strrchr (s, c);
}

char * strcat(char * s, const char * append)
{assert((s) && (append));

    char *save = s + strlen(s);

    while ((*save++ = *append++));

    return(save);
}

char * strchr(const char * p, int ch)
{assert(p);

    char c = ch;

    for (;; ++p) {

        if (*p == c) {
            return ((char *)p);
        }

        if (*p == '\0') {
            return (NULL);
        }
    }
}

int strcmp(const char * s1, const char * s2)
{assert((s1) && (s2));

    while (*s1 == *s2++) {
        if (*s1++ == '\0') {
            return (0);
        }
    }

    return (*(const unsigned char *)s1 - *(const unsigned char *)(s2 - 1));
}

char * strcpy(char * dst, const char * src)
{assert((dst) && (src));

    char *save = dst;

    for (; (*dst = *src); ++src, ++dst);

    return(save);
}

size_t strlen(const char *s)
{assert(s);

    const char *sc;

    for (sc = s; *sc != '\0'; ++sc) {
        /* nothing */;
    }

    return sc - s;
}

char * strncat(char * dst, const char * src, size_t n)
{assert((dst) && (src));

	if (n != 0) {

		char *d = dst;
		const char *s = src;

		while (*d != 0) {
			d++;
        }

		do {
			if ((*d = *s++) == 0) {
				break;
            }
			d++;
		} while (--n != 0);

		*d = 0;
	}

	return (dst);
}

int strncmp(const char * s1, const char * s2, size_t n)
{assert((s1) && (s2));

    if (n == 0) {
        return (0);
    }

    do {

        if (*s1 != *s2++) {
            return (*(const unsigned char *)s1 -
                    *(const unsigned char *)(s2 - 1));
        }

        if (*s1++ == '\0') {
            break;
        }

    } while (--n != 0);

    return (0);
}

char * strncpy(char * dst, const char * src, size_t n)
{assert((dst) && (src));

    for (; n--; dst++, src++) {
        if (!(*dst = *src)) {
            char *ret = dst;
            while (n--)
                *++dst = '\0';
            return (ret);
        }
    }

    return (dst);
}

size_t strcspn(const char *s, const char *reject)
{assert((s) && (reject));

    const char *p;
    const char *r;
    size_t count = 0;

    for (p = s; *p != '\0'; ++p) {

        for (r = reject; *r != '\0'; ++r) {

            if (*p == *r) {
                return count;
            }
        }
        ++count;
    }
    return count;
}

char * strnstr(const char *s, const char *find, size_t slen)
{assert((s) && (find));

    char c, sc;
    size_t len;

    if ((c = *find++) != '\0') {

        len = strlen(find);
        do {
            do {
                if (slen-- < 1 || (sc = *s++) == '\0') {
                    return (NULL);
                }
            } while (sc != c);

            if (len > slen) {
                return (NULL);
            }
        } while (strncmp(s, find, len) != 0);
        s--;
    }
    return ((char *)s);
}

char * strpbrk(const char *s1, const char *s2)
{assert((s1) && (s2));

    const char *scanp;
    int c, sc;

    while ((c = *s1++) != 0) {
        for (scanp = s2; (sc = *scanp++) != '\0';) {
            if (sc == c) {
                return ((char *)(s1 - 1));
            }
        }
    }
    return (NULL);
}

char * strrchr(const char * p, int ch)
{assert(p);

	char *save;
	char c = ch;

	for (save = NULL;; ++p) {

		if (*p == c) {
			save = (char *)p;
        }

		if (*p == '\0') {
			return (save);
        }
	}
}

char * strsep(char ** stringp, const char * delim)
{assert((stringp) && (delim));

    char *s;
    const char *spanp;
    int c, sc;
    char *tok;

    if ((s = *stringp) == NULL) {
        return (NULL);
    }

    for (tok = s;;) {
        c = *s++;
        spanp = delim;

        do {

            if ((sc = *spanp++) == c) {

                if (c == 0) {
                    s = NULL;
                } else {
                    s[-1] = 0;
                }

                *stringp = s;

                return (tok);
            }
        } while (sc != 0);
    }
}

size_t strspn(const char *s, const char *charset)
{assert((s) && (charset));

    const char *p;
    const char *a;
    size_t count = 0;

    for (p = s; *p != '\0'; ++p) {

        for (a = charset; *a != '\0'; ++a) {
            if (*p == *a) {
                break;
            }
        }

        if (*a == '\0') {
            return count;
        }

        ++count;
    }

    return count;
}

char * strstr(const char * s, const char * find)
{assert((s) && (find));

    char c, sc;
    size_t len;

    if ((c = *find++) != '\0') {

        len = strlen(find);
        do {
            do {
                if ((sc = *s++) == '\0') {
                    return (NULL);
                }
            } while (sc != c);
        } while (strncmp(s, find, len) != 0);
        s--;
    }
    return ((char *)s);
}

static char * __strtok_r(char *s, const char *delim, char **last)
{
	char *spanp, *tok;
	int c, sc;

	if (s == NULL && (s = *last) == NULL) {
		return (NULL);
    }

	/*
	 * Skip (span) leading delimiters (s += strspn(s, delim), sort of).
	 */
cont:
	c = *s++;
	for (spanp = (char *)delim; (sc = *spanp++) != 0;) {
		if (c == sc) {
			goto cont;
        }
	}

	if (c == 0) {		/* no non-delimiter characters */
		*last = NULL;
		return (NULL);
	}
	tok = s - 1;

	/*
	 * Scan token (scan for delimiters: s += strcspn(s, delim), sort of).
	 * Note that delim must have one NUL; we stop if we see that, too.
	 */
	for (;;) {
		c = *s++;
		spanp = (char *)delim;
		do {
			if ((sc = *spanp++) == c) {
				if (c == 0) {
					s = NULL;
				} else {
					s[-1] = '\0';
                }
				*last = s;
				return (tok);
			}
		} while (sc != 0);
	}
}

char * strtok(char *s, const char *delim)
{assert((s) && (delim));

	static char *last;

	return (__strtok_r(s, delim, &last));
}
