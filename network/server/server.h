#include <stdint.h>
struct server_recv_t {
	int id;
	unsigned char *buf;
	int len;
};
int server_init(int sock);
int server_open();
int server_close();

int server_add_module(int mid, int(*proc)(struct server_recv_t*));
int server_rm_module(int mid);
int server_add_timer(int id, int sec);
int server_rm_timer(int id);

int server_send(int id, unsigned char *buf, int size);
int server_run();
