// link with Ws2_32.lib

#include <winsock2.h>
#include <stdio.h>
///#include <ws2tcpip.h>

int main() {
    int					iResult = 0;
	WSADATA				wsaData ;
	SOCKET				sock = INVALID_SOCKET;
	struct sockaddr_in	addr;
	struct _WSABUF		buf;
	char 				buffer[1000];
	DWORD 				dwFlag = 0,dwLen;



	buf.buf = buffer;
	buf.len = sizeof(buffer);
	
	//printf("wsadata %d\n",sizeof(struct WSAData));
	iResult = WSAStartup(MAKEWORD(2, 2)/*0x0202*/, &wsaData);
    if (iResult != 0) {
        //wprintf(L"WSAStartup failed: %d\n", iResult);
        return -1;
    }


	sock = WSASocketA(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if(sock == INVALID_SOCKET) {
		//printf("WSASocket error: %d\n",WSAGetLastError());
		return -2;
	}
	//printf("addr %d\n",sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(5000);
	addr.sin_addr.S_un.S_addr = 0x100007F;//inet_addr("127.0.0.1");

	if(SOCKET_ERROR == WSAConnect(sock,(struct sockaddr*)&addr,sizeof(addr),NULL,NULL,NULL,NULL)) {
		printf("WSAConnect error: %d\n",WSAGetLastError());
		return -3;
	}
	//10051 网络不可达
	//10060 链接超时
	//10061 服务器不在线
	
	while(SOCKET_ERROR != (iResult=WSARecv(sock,&buf,1,&dwLen,&dwFlag,NULL,NULL)) && dwLen > 0)
	{
		buffer[dwLen] = 0;
		puts(buffer);
		printf("len = %d\n",dwLen);
	}
	if(iResult == SOCKET_ERROR) {
		///printf("WSARecv error: %d\n",WSAGetLastError());
		return -4;
	}
	return 0;
}

