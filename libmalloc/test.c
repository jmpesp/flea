#include "include/malloc.h"
#include <stdio.h>
#include <string.h>
int main() {
	init_malloc(1024);
	char *p1,*p2,*p3,*p4;
	show_alloc_mem();
  	p1 = malloc(50);
	show_alloc_mem();
  	p2 = malloc(50);
	show_alloc_mem();
  	p3 = malloc(50);
	show_alloc_mem();
  	p4 = malloc(50);
	show_alloc_mem();
	free(p1);
	free(p2);
	free(p3);
	free(p4);
	show_alloc_mem();
  	p1 = malloc(50);
	show_alloc_mem();
	strcpy(p1,"helloworld\n");
	printf(p1);
}
